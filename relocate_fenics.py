'''
Generate script to adjust FEnics.app rpath on OSX El Capitan

Applies to fenics-1.6.0-osx10.10.dmg

the CERT must be set to a name of valid code-signign certificate

see
http://bd808.com/blog/2013/10/21/creating-a-self-signed-code-certificate-for-xcode/
for instructions how a self-signed certificate can be generated

usage:
   cd /Applications/FEnics.app
   python relocate_fenics.py > relocate_fenics.sh

   # check that everything looks ok in the generated script
   sh relocate_fenics.sh

   # final step requires to create a copy of the system Python executable that is not restricted by System Integrity Protection
   # to allow compiled forms to be loaded back into the interpretter

   # make sure ~/bin exists

   $ cp /System/Library/Frameworks/Python.framework/Versions/2.7/Resources/Python.app/Contents/MacOS/Python ~/bin/python

   now modify your ~/.profile file so our unrestricted Python is found first
   export PATH=~/bin:$PATH

   # optional step is to re-sign the python itself to avoid firewall repeating the permission prompt on every execution
   codesign -f -s my_certificate ~/bin/python

'''
import os

CERT = '' # must be a string representing valid certificate
assert CERT,"CERT must be a name of a valid certificate"

libs = {}
exes = []
root = os.getcwd()
sign = []

# find all Mach-64 executables
for name in os.popen('''find . \( -type f -o -type l \) -perm +111 | xargs file | grep 'Mach-O 64-bit' '''):
    name = name.strip().split(' ')[0].rstrip(':')
    libs[os.path.basename(name)] = name.lstrip('./')
    exes.append(name)

# generate the install_name_tool list
for exe in exes:
    deps = os.popen('''otool -L ''' + exe)
    l = 0
    for dep in deps:
        dep = dep.split()[0].strip()
        l += 1
        if l > 2 and not dep.startswith('/usr/') and not dep.startswith('/System/') and not dep.startswith(root): # skip line 1 and 2
            print 'install_name_tool -change %s %s %s' % (dep,os.path.join(root,libs[os.path.basename(dep)]),exe)
            if not exe in sign:
                sign.append(exe)


# generate the re-signing commands
for s in sign:
    print 'codesign -f -s %s %s  ' % (CERT, s)
